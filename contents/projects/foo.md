---
preview: https://loremflickr.com/420/320
hero: https://loremflickr.com/1920/1080
title: Foo
client: Bar
excerpt: Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod
tempor incididunt ut labore et dolore magna aliqua.
lead_separator: <!--more-->
published_at: 2021-02-19
---
Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod
tempor incididunt ut labore et dolore magna aliqua.

<!--more-->

# Heading 1 #
Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod
tempor incididunt ut labore et dolore magna aliqua.

## Heading 2 ##
Excepteur sint occaecat cupidatat non
proident, sunt in culpa qui officia deserunt mollit anim id est laborum.

### Text ###
Ut enim ad minim veniam,
quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo
consequat. 

### Image ###
Duis aute irure dolor in reprehenderit in voluptate velit esse
cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non
proident, sunt in culpa qui officia deserunt mollit anim id est laborum.
![image-1]
{credits}
Source: loremflickr.com
{/credits}

### Slideshow Default ###
{slideshow}
![image-1]
![image-1]
![image-1]
{/slideshow}

### Slideshow Preferences ###
{slideshow:cats}
![image-2]
![image-2]
![image-2]
{/slideshow}

### 4:3 Video ###
Duis aute irure dolor in reprehenderit in voluptate velit esse
cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non
proident, sunt in culpa qui officia deserunt mollit anim id est laborum.
{vimeo:12345}

### 16:9 Video ###
Excepteur sint occaecat cupidatat non
proident, sunt in culpa qui officia deserunt mollit anim id est laborum.
{vimeo:288344114}

### Custom Size Video ###
Lorem ipsum
{vimeo:884010243}

### Separated elements 1 ###
Some brief description  
**Bold:** Design
**Bold:** *italic*
___
### Separated elements 2 ###
Some brief description  
**Sector:** Design
**Year:** 2021

### Grid ###
@grid
![image-2]
![image-2]
![image-2]
@endgrid

[image-1]: https://loremflickr.com/320/160
[image-2]: https://loremflickr.com/160/226