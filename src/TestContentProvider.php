<?php

namespace Folio\Contents;

class TestContentProvider
{
	public function getBasePath()
	{
		return realpath(__DIR__ . '/../contents');
	}
}